package mvanbrummen.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import lombok.var;
import org.imgscalr.Scalr;
import software.amazon.awssdk.http.urlconnection.UrlConnectionHttpClient;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.HeadObjectRequest;

import javax.imageio.ImageIO;
import javax.inject.Named;
import java.util.concurrent.CompletableFuture;

@Named("test")
public class TestLambda implements RequestHandler<InputObject, OutputObject> {
    private static final String TEST_KEY = "assets/perm/22cdhdhxjui6pa7l76n7zfbonu";
    private static final String SOURCE_BUCKET = "dasless-images";

    private final S3Client s3Client = S3Client.builder()
            .region(Region.AP_SOUTHEAST_2)
            .httpClient(UrlConnectionHttpClient.create())
            .build();

    @Override
    public OutputObject handleRequest(InputObject input, Context context) {
        try {
            s3Client.headObject(HeadObjectRequest.builder()
                    .bucket(SOURCE_BUCKET)
                    .key(TEST_KEY)
                    .build());

            var result = s3Client.getObject(GetObjectRequest.builder()
                    .bucket(SOURCE_BUCKET)
                    .key(TEST_KEY)
                    .build());

            var img = ImageIO.read(result);

            var resized = Scalr.resize(img, Scalr.Method.QUALITY, Scalr.Mode.FIT_EXACT, 700, 500);

            CompletableFuture.runAsync(() -> {
                s3Client.headObject(HeadObjectRequest.builder()
                        .bucket(SOURCE_BUCKET)
                        .key("/result/" + TEST_KEY)
                        .build());
            });

            return new OutputObject().setResult("hello, world! " + result.response().eTag());
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw new RuntimeException(e);
        }
    }
}
